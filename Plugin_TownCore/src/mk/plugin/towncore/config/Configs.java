package mk.plugin.towncore.config;

import java.util.List;
import java.util.Map;

import org.bukkit.World;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import mk.plugin.towncore.item.ItemModel;

public class Configs {
	
	private static Map<String, ItemModel> models = Maps.newHashMap();
	private static List<String> pvpWorlds = Lists.newArrayList();
		
	public static ItemModel getModel(String id) {
		return models.getOrDefault(id, null);
	}
	
	public static boolean isPvPWorld(World w) {
		return pvpWorlds.contains(w.getName());
	}
	
}

package mk.plugin.towncore.listener;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;

import mk.plugin.towncore.config.Configs;
import mk.plugin.towncore.damage.Damage;
import mk.plugin.towncore.damage.DamageType;
import mk.plugin.towncore.damage.Damages;
import mk.plugin.towncore.event.ItemToggleEvent;
import mk.plugin.towncore.item.EquipmentType;
import mk.plugin.towncore.item.Item;
import mk.plugin.towncore.item.ItemData;
import mk.plugin.towncore.item.ItemModel;
import mk.plugin.towncore.item.Items;
import mk.plugin.towncore.player.TPlayers;
import mk.plugin.towncore.stat.Stat;
import mk.plugin.towncore.utils.Utils;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.ComponentBuilder;

public class WeaponListener {
	
	
	private static Map<String, Long> cooldownAttack = new HashMap<String, Long> ();	
	
	public static boolean isCooldownAttack(Player player) {
		if (!cooldownAttack.containsKey(player.getName())) return false;
		if (cooldownAttack.get(player.getName()) < System.currentTimeMillis()) return false;
		return true;
	}
	
	public static void setCooldownAttack(Player player, long timeMilis, ItemStack item) {
		if (item != null) player.setCooldown(item.getType(), Long.valueOf(timeMilis / 50).intValue());
		cooldownAttack.put(player.getName(), System.currentTimeMillis() + timeMilis);	
	}
	
	public static void removeCooldownAttack(Player player) {
		cooldownAttack.remove(player.getName());
	}
	
	@EventHandler
	public void onToggle(ItemToggleEvent e) {
		Player player = e.getPlayer();
		ItemStack is = e.getItemStack();
		Item item = e.getItem();
		ItemModel model = Configs.getModel(item.getModel());
		
		if (model.getType() == EquipmentType.WEAPON) {
			
			// Check durability
			ItemData data = item.getData();
			if (data.getDurability() <= 0) {
				player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new ComponentBuilder("§cVũ khí có độ bền bằng 0, cần phải sữa chữa").create());
				return;
			}
			
			// Check cool-down
			if (isCooldownAttack(player)) return;
			setCooldownAttack(player, Double.valueOf(TPlayers.getStatValue(player, Stat.ATTACK_SPEED) * 1000).longValue(), is);
			
			// Shoot
			if (is.getType() == Material.BOW) {
				Items.shoot(item);
			}
			// Attack
			else {
				double range = model.getRange();
				LivingEntity target = Utils.getTarget(player, range);
				if (target == null) return;
				Damages.damage(player, target, new Damage(TPlayers.getStatValue(player, Stat.DAMAGE), DamageType.ATTACK), 0);
			}
			
			// Minus durability
			if (Configs.isPvPWorld(player.getWorld())) return;
			data.setDurability(Math.max(data.getDurability() - 1, 0));
			player.getInventory().setItemInMainHand(Items.write(player, is, item));
		}
	}
	
	
}

package mk.plugin.towncore.listener;

import org.bukkit.Bukkit;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerAnimationEvent;
import org.bukkit.event.player.PlayerAnimationType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import mk.plugin.towncore.damage.Damages;
import mk.plugin.towncore.event.ItemToggleEvent;
import mk.plugin.towncore.item.Item;
import mk.plugin.towncore.item.Items;
import mk.plugin.towncore.main.MainTownCore;

public class ItemListener {
	
	public final String DAMAGE_TAG = "item.damagetag";
	public final String INTERACT_TAG = "item.interact";

	/*
	 *  Animation = last
	 */
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		Player player = e.getPlayer();
		if (e.getAction() == Action.LEFT_CLICK_AIR) {
			player.setMetadata(INTERACT_TAG, new FixedMetadataValue(MainTownCore.get(), ""));
		}
	}
	
	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e) {
		if (e.getDamager() instanceof Player && e.getEntity() instanceof LivingEntity) {
			Player player = (Player) e.getDamager();
			LivingEntity target = (LivingEntity) e.getEntity();
			if (Damages.hasDamage(target)) return;
			player.setMetadata(DAMAGE_TAG, new FixedMetadataValue(MainTownCore.get(), ""));
		}
	}
	
	@EventHandler
	public void onAnimation(PlayerAnimationEvent e) {
		Player player = e.getPlayer();
		if (player.isSneaking()) return;
		if (e.getAnimationType() == PlayerAnimationType.ARM_SWING) {
			// Check
			boolean hasDamage = player.hasMetadata(DAMAGE_TAG);
			boolean hasInteract = player.hasMetadata(INTERACT_TAG);
			
			// Remove tags
			player.removeMetadata(DAMAGE_TAG, MainTownCore.get());
			player.removeMetadata(INTERACT_TAG, MainTownCore.get());
			
			// Event
			if (hasDamage || hasInteract) {
				// Call event
				ItemStack is = player.getInventory().getItemInMainHand();
				if (Items.is(is)) {
					Item item = Items.read(is);
					Bukkit.getPluginManager().callEvent(new ItemToggleEvent(player, item, is));
				}
			}
		}
	}
	
}

package mk.plugin.towncore.player;

public class TPlayerData {
	
	private long exp;
	
	public TPlayerData(long exp) {
		this.exp = exp;
	}
	
	public long getExp() {
		return this.exp;
	}
	
}

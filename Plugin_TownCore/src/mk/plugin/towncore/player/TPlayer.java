package mk.plugin.towncore.player;

public class TPlayer {
	
	private TPlayerState state;
	private TPlayerData data;
	
	public TPlayer(TPlayerState state, TPlayerData data) {
		this.state = state;
		this.data = data;
	}
	
	public TPlayerState getState() {
		return this.state;
	}
	
	public TPlayerData getData() {
		return this.data;
	}
	
}

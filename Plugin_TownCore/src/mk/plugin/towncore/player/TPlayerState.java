package mk.plugin.towncore.player;

import java.util.Map;

import mk.plugin.towncore.stat.Stat;

public class TPlayerState {

	private Map<Stat, Integer> stats;
	
	public TPlayerState(Map<Stat, Integer> stats) {
		this.stats = stats;
	}
	
	public Map<Stat, Integer> getStats() {
		return this.stats;
	}
	
	public void setStats(Map<Stat, Integer> stats) {
		this.stats = stats;
	}
	
}

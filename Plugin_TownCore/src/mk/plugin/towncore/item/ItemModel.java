package mk.plugin.towncore.item;

import java.util.Map;

import org.bukkit.enchantments.Enchantment;

import mk.plugin.towncore.stat.Stat;

public class ItemModel {
	
	private ItemTexture texture;
	private EquipmentType type;
	private String name;
	private Tier tier;
	private double range;
	private String desc;
	private int socketHole;
	private Map<Stat, Integer> stats;
	private Map<Enchantment, Integer> enchants;
	
	public ItemModel(ItemTexture texture, EquipmentType type, String name, Tier tier, double range, String desc, int socketHole, Map<Stat, Integer> stats, Map<Enchantment, Integer> enchants) {
		this.type = type;
		this.tier = tier;
		this.range = range;
		this.name = name;
		this.desc = desc;
		this.socketHole = socketHole;
		this.stats = stats;
		this.enchants = enchants;
	}
	
	public ItemTexture getTexture() {
		return this.texture;
	}
	
	public EquipmentType getType() {
		return this.type;
	}
	
	public String getName() {
		return this.name;
	}
	
	public Tier getTier() {
		return this.tier;
	}
	
	public double getRange() {
		return this.range;
	}
	
	public String getDesc() {
		return this.desc;
	}
	
	public int getSocketHole() {
		return this.socketHole;
	}
	
	public Map<Stat, Integer> getStats() {
		return this.stats;
	}
	
	public Map<Enchantment, Integer> getEnchants() {
		return this.enchants;
	}
	
}

package mk.plugin.towncore.item;

import org.bukkit.Material;

public class ItemTexture {
	
	private Material material;
	private int data;
	
	public ItemTexture(Material material, int data) {
		this.material = material;
		this.data = data;
	}
	
	public Material getMaterial() {
		return this.material;
	}
	
	public int getData() {
		return this.data;
	}
	
}

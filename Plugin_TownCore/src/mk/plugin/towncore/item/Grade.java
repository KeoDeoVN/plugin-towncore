package mk.plugin.towncore.item;

public enum Grade {
	
	I(1),
	II(2),
	III(3),
	IV(4),
	V(5)
	;
	
	public static final String ICON = "☆";
	
	private int value;
	
	private Grade(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return this.value;
	}
	

}

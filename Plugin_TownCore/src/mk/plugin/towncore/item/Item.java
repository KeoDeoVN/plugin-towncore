package mk.plugin.towncore.item;

public class Item {
	
	private String model;
	private ItemData data;
	
	public Item(String model, ItemData data) {
		this.model = model;
		this.data = data;
	}
	
	public String getModel() {
		return this.model;
	}
	
	public ItemData getData() {
		return this.data;
	}
	
	public void setData(ItemData data) {
		this.data = data;
	}
	
}

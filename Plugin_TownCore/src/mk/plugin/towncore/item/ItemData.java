package mk.plugin.towncore.item;

import java.util.List;

import mk.plugin.towncore.socket.Socket;

public class ItemData {
	
	private int gradeExp;
	private int level;
	private int durability;
	private List<Socket> sockets;
	
	public ItemData(int gradeExp, int level, int durability, List<Socket> sockets) {
		this.gradeExp = gradeExp;
		this.level = level;
		this.durability = durability;
		this.sockets = sockets;
	}
	
	public int getGradeExp() {
		return this.gradeExp;
	}
	
	public void setGradeExp(int gradeExp) {
		this.gradeExp = gradeExp;
	}
	
	public int getLevel() {
		return this.level;
	}
	
	public void setLevel(int level) {
		this.level = level;
	}
	
	public int getDurability() {
		return this.durability;
	}
	
	public void setDurability(int durability) {
		this.durability = durability;
	}
	
	public List<Socket> getSockets() {
		return this.sockets;
	}
	
}

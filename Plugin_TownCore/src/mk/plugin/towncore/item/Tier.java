package mk.plugin.towncore.item;

public enum Tier {
	
	SO_CAP("Sơ cấp", "§f"),
	TRUNG_CAP("Trung cấp", "§9"),
	CAO_CAP("Cao cấp", "§c"),
	CUC_PHAM("Cực phẩm", "§6"),
	HUYEN_THOAI("Huyền thoại", "§a");
	
	private String color;
	private String name;
	
	private Tier(String name, String color) {
		this.color = color;
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getColor() {
		return this.color;
	}
	
}

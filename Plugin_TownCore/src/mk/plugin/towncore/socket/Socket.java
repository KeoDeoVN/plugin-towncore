package mk.plugin.towncore.socket;

import mk.plugin.towncore.item.Tier;
import mk.plugin.towncore.stat.Stat;

public class Socket {
	
	private String name;
	private String desc;
	private Tier tier;
	private Stat stat;
	private int buff;
	
	public Socket(String name, String desc, Tier tier, Stat stat, int buff) {
		this.name = name;
		this.desc = desc;
		this.tier = tier;
		this.stat = stat;
		this.buff = buff;
	}
	
	public String getName() {
		return this.name;
	}	
	
	public String getDesc() {
		return this.desc;
	}
	
	public Tier getTier() {
		return this.tier;
	}
	
	public Stat getStat() {
		return this.stat;
	}
	
	public int getBuff() {
		return this.buff;
	}
	
}

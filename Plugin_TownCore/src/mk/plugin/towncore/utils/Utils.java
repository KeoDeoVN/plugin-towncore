package mk.plugin.towncore.utils;

import java.text.DecimalFormat;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import com.google.common.collect.Sets;

public class Utils {
	
	public static double round(double i) {
		return Double.valueOf(new DecimalFormat("#.##").format(i).replace(",", "."));
	}
	
	public static LivingEntity getTarget(Player source, double range) {
		List<Block> blocksInSight = source.getLineOfSight(Sets.newHashSet(Material.AIR), Double.valueOf(range).intValue());
		List<Entity> nearEntities = source.getNearbyEntities(range, range, range);
		
		if (blocksInSight != null && nearEntities != null) {
			for (Block block : blocksInSight) {
				int xBlock = block.getX();
				int yBlock = block.getY();
				int zBlock = block.getZ();

				for (Entity entity : nearEntities) {
					if (!(entity instanceof LivingEntity)) continue;
					Location entityLocation = entity.getLocation();
					int xEntity = entityLocation.getBlockX();
					int yEntity = entityLocation.getBlockY();
					int zEntity = entityLocation.getBlockZ();
					if (xEntity == xBlock && (Math.abs(yBlock - yEntity) < 2) && zEntity == zBlock) {
						return (LivingEntity) entity;
					}
					
				}
			}
		}
		return null;
	}
	
}
